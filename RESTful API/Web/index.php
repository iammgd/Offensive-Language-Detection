<html>
<head lang="en">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>  
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="style.css" rel="stylesheet">
</head>
   <body>
   	<header>
   		<div class="container" id="judul">
   			<h2>Demo Sistem Pendeteksi Kalimat Umpatan di Media Sosial dengan Model Jaringan Saraf Tiruan</h2>
   			<h4>Tim PKM-KC 2019 Universitas Pertamina</h4>
   		</div>
   	</header>
	   	<div id="konten"class="row">
		   	<div id="cara_kerja" class="col-lg-6">
		   		<h5 class="text-capitalize">Cara Kerja:</h5>
				        <p>Menentukan kalimat atau frasa umpatan (Offensive) atau bukan umpatan (Non-offensive) pada kalimat atau frasa yang mengandung nama binatang (mis, babi, anjing, monyet, dll), kalimat atau frasa yang melecehkan intelektual seseorang (goblok), dan kata - kata kasar lainnya.</p>
		   	</div>
		   	<div id="cara_kerja" class="col-lg-6">
		   		<h5 class="text-capitalize">Contoh:</h5>
		   		<p id="contoh_cara_kerja">Text : Muka kamu Seperti <strong>babi</strong> hutan! (<strong>Offensive</strong>)</p>
				        <p>Text : <strong>Babi</strong> kamu lucu sekali (<strong>Non-Offensive</strong>)</p>
		   	</div>
	   	</div>
 
   		<div id="konten" class="row">
	        <div class="col-lg-6">
		        <form id="form-post" method = "POST">
		        	<label class="font-weight-bold">Input: </label> 
		        	<textarea type ="text" name="teks" id="user_input" class="md-textarea form-control" rows="3" placeholder="Tuliskan Kalimat.."></textarea>
		         	<!-- <textarea type ="text" name = "teks" id="user_input"></textarea> -->
		        	<button type = "submit" class="btn btn-primary font-weight-bold" id="tombol">CHECK</button> 
		      	</form>
	      	</div>	
	      	<div class="col-lg-6">
	       		<label class="font-weight-bold">Output: </label>
	       		<textarea id="output-text" class="" rows="3"></textarea>
	  			<!-- <textarea id="output-text"></textarea> -->
  			</div>
  		</div>
	<!-- Footer -->
		<footer id="desc" class="page-footer font-small lighten-3 pt-4">

		  <!-- Footer Text -->
		  <div id="kontenfooter" class="text-center text-md-left">

		    <!-- Grid row -->
		    <div class="row">

		      <!-- Grid column -->
		      <div class="col-md-6 mt-md-0 mt-3">

		        <!-- Content -->
		        
		        <h5 class="text-capitalize font-weight-bold">Cara Penggunaan :</h5>
		        <p>Model yang digunakan adalah model NN dan diproduksi sebagai RESTful API yang dapat diakses melalui <strong id="link_API">http://pkm-kc-2019.herokuapp.com/predict/[text_input]</strong> di platform manapun.</p>
		      </div>
		      <!-- Grid column -->

		      <hr class="clearfix w-100 d-md-none pb-3">

		      <!-- Grid column -->
		      <div class="col-md-6 mb-md-0 mb-3">

		        <!-- Content -->
		        
		        <h5 class="text-uppercase font-weight-bold">Lebih Lanjut :</h5>
		        <p>Github : <a href="https://github.com/1ab94ee/Offensive-Language-Detection">Offensive Language Detection</a></p>
		        <p>Contributor : <a target="_blank" href="https://www.linkedin.com/in/h2sahrul/">Sahrul</a>, <a target="_blank" href="https://www.linkedin.com/in/ahmadfauzanrahman/">Ahmad Fauzan Rahman</a>, dan <a target="_blank" href="https://www.linkedin.com/in/muhammad-dzaky-/">Muhammad Dzaky Normansyah</a>.</p>
		        <p>Metode Pengembangan :</p>

		      </div>
		      <!-- Grid column -->

		    </div>
		    <!-- Grid row -->

		  </div>
		  <!-- Footer Text -->

		  <!-- Copyright -->
		  <div class="footer-copyright text-center py-3">
		  	Proyek penelitian ini didanai oleh Kementrian Riset, Teknologi, dan Pendidikan Tinggi Republik Indonesia melalui Program Kreativitas Mahasiswa Bidang Karsa Cipta (PKM-KC) 2019 
		  </div>
		  <!-- Copyright -->

		</footer>
		<!-- Footer -->
  <script type="text/javascript">
	$(document).ready(function(){
			// console.log('tes')
		$('#form-post').on('submit', function(event){
			name = $('#user_input').val();
			$.ajax({
				url: 'api.php',
				method: 'POST',
				data: {teks : name}
			}).done(function(data){
				// console.log(JSON.parse(data).data[0])
				const content = JSON.parse(data).data[0]
				$('#output-text').val(`${content.class}\nConfidence = ${content.confidence}`)
				if (content.class === 'Offensive') {
					$("#output-text").addClass('text-danger')
				} else if (content.class === 'Non-Offensive'){
					$("#output-text").addClass('text-success')
				}
			})
			event.preventDefault();
		})
		$('#user_input').keydown(function() {
			var message = $("textarea").val();
			if (event.keyCode == 13) {
			if (message == "") {
			alert("Tolong tuliskan kalimat/kata");
			}
			$("textarea").val('');
			return false;
			}
			});
	});
  </script>
   </body>

</html>
