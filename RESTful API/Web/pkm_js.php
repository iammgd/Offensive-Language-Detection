<html>
<head lang="en">
  <meta charset="UTF-8">
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
   <body>
   	<div class="col-lg-12">
   		<h1 style="text-align: center; padding-bottom: 50px">Pendeteksi Kalimat Umpatan</h1>
   		
   	</div>
    <div class ="row">
    	<div class="col-lg-6">
        	<form id="form-post" method = "POST">
         		<label for="input_text"><b>Input: </b></label> 
         		<textarea type ="text" name = "teks" id="user_input"></textarea>
      	</div>
	    <div class="col-lg-6">	
	       	<label for="output_text"><b>Output: </b></label>
	  		<textarea id="output-text"></textarea>
  		</div>
  	</div>
  	<div class="row" style="padding-left: 50px">
  		<input type="submit" id="btn-stem" class="btn-primary" value="SEND!">
      		</form>
  	</div>
  <script type="text/javascript">

	$(document).ready(function(){
			// console.log('tes')
		$('#form-post').on('submit', function(event){
			name = $('#user_input').val();
				console.log(name)
			var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://pkm-kc-2019.herokuapp.com/predict/"+name,
  "method": "GET"
}

$.ajax(settings).done(function (response) {
  console.log(response);
  				$('#output-text').val(response);

});	  
			event.preventDefault();
		})
	});
  </script>
   </body>
</html>
<!-- <html>
<head>
	<link rel="stylesheet" href="css/main.css">
</head>
   <body>
            <form method = "POST" action = "api.php">
         <label>Teks: <textarea type = "text" name = "teks"></textarea></label>
         <button type = "submit">SEND</button> 
      </form>
      
   </body>
</html> -->